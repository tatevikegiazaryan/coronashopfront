import axios from 'axios'
const url  = 'http://corona.test/api';
let token = localStorage.getItem('token');
export const apiClient = axios.create({
  baseURL: url,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${ token }`
  },
});
export default apiClient;
