import axios from '../../../axios';
export const actions = {
  LOGIN({commit}, data) {
    return axios.post('/login', {
      email: data.email,
      password: data.password,
    })
      .then((response) => {
        console.log(response);
        const token = response.data.token;
        commit('SET_ERROR', null);
        localStorage.setItem('token', token);
        location.href = '/catalog';
        return response;
      })
      .catch((error) => {
        commit('SET_ERROR', true);
        console.log(error);
        return error;
      })
  },

  REGISTER({commit}, data) {
    return axios.post('/register', {
      name:data.name,
      email:data.email,
      password:data.password,
    })
      .then((response) => {
        location.href = '/login';
        console.log(response);
        commit('SET_ERROR', null);
        return response;

      })
      .catch((error) => {
        commit('SET_ERROR', true);
        console.log(error);
        return error;
      })
  },
  LOGOUT() {
    return axios.post('/logout', )
      .then((response) => {
        console.log(response);
        location.href = '/login';
        localStorage.removeItem('token');
        return response;
      })
      .catch((error) => {
        console.log(error);
        return error;
      })
  },

};
