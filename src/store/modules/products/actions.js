import axios from '../../../axios';

export const actions = {
  GET_PRODUCTS_FROM_API({commit}) {
    return axios.get('/corona')
      .then((response) => {
        console.log(response);
        commit('SET_PRODUCTS_TO_STATE', response.data);
        return response;
      })
      .catch((error) => {
        console.log(error);
        return error;
      })
  },

  CREATE (content, formData) {
    return axios.post('/create', formData)
      .then((response) => {
        console.log(response);
        location.href = '/catalog';
        return response;
      })
      .catch((error) => {
        console.log(error);
        return error;
      })
  },

  GET_SEARCH_VALUE_TO_VUEX({commit}, value) {
    commit('SET_SEARCH_VALUE_TO_VUEX', value)
  },

  DELETE_CART_All({ commit }) {
    commit('DELETE_CART_All')
  },
  DELETE_ALL_WISH_LIST({ commit }) {
    commit('DELETE_ALL_WISH_LIST')
  },

  DELETE_FROM_CART({ commit }, index) {
    commit('DELETE_FROM_CART', index)
  },

  DELETE_FROM_WISH_LIST({ commit },index) {
    commit('DELETE_FROM_WISH_LIST', index)
  },

  ADD_TO_CART({ commit }, product) {
    commit('SET_CART', product);
  },
  ADD_TO_CART_FROM_WISH_LIST({ commit }, product, index) {
    commit('SET_CART', product);
    commit('DELETE_FROM_WISH_LIST', index);
  },

  ADD_TO_HEART({ commit }, product) {
    commit('SET_HEART', product);
  },

  INCREMENT_CART_ITEM({commit}, index) {
    commit('INCREMENT_CART_ITEM', index)
  },

  DECREMENT_CART_ITEM({commit}, index) {
    commit('DECREMENT_CART_ITEM', index)
  },
};


