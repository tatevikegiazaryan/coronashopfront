export const getters = {
  CART(state) {
    return state.cart;
  },
  HEART(state) {
    return state.heart;
  },
  SEARCH_VALUE(state) {
    return state.searchValue;
  },
  PRODUCTS (state) {
    return state.products;
  },
};
