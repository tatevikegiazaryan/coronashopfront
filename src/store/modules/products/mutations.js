export const mutations = {
  DELETE_CART_All(state) {
    return state.cart.splice(0);
  },
  DELETE_ALL_WISH_LIST(state) {
    return state.heart.splice(0);
  },

  DELETE_FROM_CART(state, index) {
    return state.cart.splice(index,1)
  },

  DELETE_FROM_WISH_LIST(state, index) {
    return state.heart.splice(index,1)
  },

  SET_CART(state, product) {
    if(state.cart.length) {
      let isProductExists = false;
      state.cart.map((item) => {
        if(product.id === item.id) {
          isProductExists = true;
          item.quantity++;
        }
      });
      if(!isProductExists) {
        return state.cart.push(product);
      }
    } else {
      return state.cart.push(product);
    }
  },

  SET_PRODUCTS_TO_STATE(state, products) {
    return state.products = products;
  },

  SET_HEART(state, product) {
    if(state.heart.length) {
      let isProductExists = false;
      state.heart.map((item) => {
        if(product.id === item.id) {
          isProductExists = true;
        }
      });
      if(!isProductExists) {
        state.isHeartExists = false;
        return state.heart.push(product);
      }
    } else {
      state.isHeartExists = true;
      return state.heart.push(product);
    }
  },

  DECREMENT_CART_ITEM(state, index) {
    if(state.cart[index].quantity >1) {
      return state.cart[index].quantity--
    }
  },

  INCREMENT_CART_ITEM (state, index) {
    return state.cart[index].quantity++
  },

  SET_SEARCH_VALUE_TO_VUEX(state, value) {
    return state.searchValue = value;
  },

};
