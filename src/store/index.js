import Vue from 'vue';
import Vuex from 'vuex';
import account from './modules/account';
import products from './modules/products';
Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    account,
    products,
  },
});
