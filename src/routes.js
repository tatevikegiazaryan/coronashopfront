import VueRouter from 'vue-router'
import Catalog from './components/v-main-wrapper'
import CreateItem from './components/catalog/v-create-item'
import vCart from './components/cart/v-cart'
import vDetails from './components/catalog/v-details'
import vWishList from './components/wishList/v-wish-list'
import vLogin from './components/user/v-login'
import vRegister from './components/user/v-register'


export default new VueRouter ({
  routes: [
    {
      path: '/login',
      alias: '/',
      component: vLogin,
      name: 'login',
      meta: {
        requiresAuth: false,
      }
    },
    {
      path: '/register',
      component: vRegister,
      name: 'register',
      meta: {
        requiresAuth: false,
      }
    },
    {
      path: '/catalog',
      component: Catalog,
      name: 'catalog',
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/cart',
      component: vCart,
      name: 'cart',
      props: true,
      meta: {
        requiresAuth: true,
      }
    },

    {
      path: '/details',
      component: vDetails,
      name: 'details',
      props: true,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/wish-list',
      component: vWishList,
      name: 'heart',
      props: true,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/create',
      component:CreateItem,
      name:'create',
      meta: {
        requiresAuth: true,
      }
    },
],
  mode: 'history'
})
