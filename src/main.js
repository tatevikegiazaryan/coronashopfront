import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate);
import router from './routes'
Vue.use(VueRouter);
import { store } from './store/index.js';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/styles.scss';
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!localStorage.getItem('token')) {
      next({
        path: '/login',
       })
    } else {
      next()
    }
  }
  if (to.matched.some(record => record.meta.requiresAuth === false)) {
    if (localStorage.getItem('token')) {
      next({
        path: '/catalog',
      })
    } else {
      next()
    }
  } else {
    next()
  }
});

new Vue({
   render: h => h(App),
  router,
  store,
}).$mount('#app');
